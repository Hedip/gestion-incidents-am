<?php

namespace BugApp\Models;

use BugApp\Services\Manager;
use BugApp\Models\Engineer;
use BugApp\Models\Recorder;

class UserManager extends Manager
{
    

    public function findByEmail($email)
    {

        // Connexion à la BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT * FROM user WHERE email = :email');
        $sth->bindParam(':email', $email);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        $user = null;
        //on check si il existe en base
        if($result != null) {
            switch($result["type"]) {
                case 'recorder':
                    //client
                    $user = new Recorder();
                    $user->setId($result["id"]);
                    $user->setNom($result["nom"]);
                    $user->setEmail($result["email"]);
                    $user->setPassword($result["password"]);
                    break;
                case 'engineer':
                    //ingenieur
                    $user = new Engineer();
                    $user->setId($result["id"]);
                    $user->setNom($result["nom"]);
                    $user->setEmail($result["email"]);
                    $user->setPassword($result["password"]);
                    break;
            }
        }
        // Retour
        return $user;
    }

    public function check($user, $mdp) {
        return $user->getPassword() == $mdp;
    }







}
