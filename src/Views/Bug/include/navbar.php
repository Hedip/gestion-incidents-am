<?php

echo '
    <nav class="light-blue lighten-1" role="navigation">
        <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">Ti Kette Pro</a>
        <ul class="right hide-on-med-and-down">';

        if(isset($_SESSION['user'])) {
            echo '<li>'.$_SESSION['user']->getNom().'</li>';
            echo '<li><a href="/logout">Logout</a></li>';
        }

echo '     </ul>

        <ul id="nav-mobile" class="sidenav">
            <li><a href="#">Navbar Link</a></li>
        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
    </nav>
    ';

?>