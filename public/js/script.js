let checkboxAfficheNonCloture = document.getElementById("notclosed");
let checkboxAfficheAssigne = document.getElementById("assigned");
let searchbar = document.getElementById('search');

checkboxAfficheAssigne.addEventListener('click', filtrer);
checkboxAfficheNonCloture.addEventListener('click', filtrer);
searchbar.addEventListener('input', filtrer);

function filtrer(event) {
    let request = new XMLHttpRequest();
    let uri = "/bug/filter?";
    let params =  {
        "notclosed": false,
        "assigned": false,
        "query": searchbar.value,
    };

    switch(true) {

        case (checkboxAfficheNonCloture.checked && checkboxAfficheAssigne.checked) :
            params.notclosed = true;
            params.assigned = true;
            break;


        case checkboxAfficheAssigne.checked:
            params.assigned = true;
            break;


        case checkboxAfficheNonCloture.checked:
            params.notclosed = true;
            break;

        default:
    }

    for (const [key, value] of Object.entries(params)) {
        uri = uri + key + "=" + value + "&";
    }

    //supprime le dernier & inutile
    uri = uri.substr(0, uri.length-1);

    request.open('GET', uri);
    request.send();

    request.onreadystatechange = () => {
        if(request.readyState == XMLHttpRequest.DONE && request.status == 200) {
            //console.log(request.responseText);
            let parsedData = JSON.parse(request.responseText);
            if(parsedData.isRequestOk) {
                let listBody = document.getElementById("listbody");
                listBody.innerHTML = "";
                for(const result of Object.values(parsedData.results)) {
                    //console.log(result);
                    let createdAt = result.createdAt.date.replace(/^(\d+)-(\d+)-(\d+).*/, "$3/$2/$1");
                    let closedAt = "En cours";
                    if(result.closedAt) {
                        closedAt = result.closedAt.date.replace(/^(\d+)-(\d+)-(\d+).*/, "$3/$2/$1");
                    } else if(result.engineer != null && parsedData.connectedUserId == result.engineer.id) {
                        closedAt = `<a class="waves-effect waves-light btn-small" id="close" onclick="update(this, ${result.id})">Clôturer</a>`;
                    }
                    
                    listBody.innerHTML +=  `
                        <tr class="bug" id="id${result.id}">
                        <td>${result.id}</td>
                        <td>${result.title}</td>
                        <td>${createdAt}</td>
                        <td>${result.recorder.nom}</td>
                        <td><a class="waves-effect waves-light btn-small" href="${"/bug/show/" + result.id}"><i class="material-icons left">subject</i>détails...</a></td>
                        <td class="case-assign">${(result.engineer ? result.engineer.nom : '<a class="waves-effect waves-light btn-small" id="assign" onclick="assign(this,'+result.id+')">Assigner</a>')}</td>
                        <td class="case-close">${closedAt}</td>
                        </tr>
                    `;
                    
                }

                if (listBody.innerHTML.length == 0) {
                    listBody.innerHTML = `<td colspan="7">Aucun résultat disponible.</td>`;
                }

            }
        }
    };

}




function update(event, bugId) {
    let request = new XMLHttpRequest();
    let uri = '/bug/updateajax/' + bugId + "?";
    let params =  {
        "close": false,
        "assign": false,
    };

    switch(event.id) {

        case 'assign':
            params.assign = true;
            break;

        case 'close':
            params.close = true;
            break;

        default:
    }

    for (const [key, value] of Object.entries(params)) {
        uri = uri + key + "=" + value + "&";
    }

    //supprime le dernier & inutile
    uri = uri.substr(0, uri.length-1);

    request.open('GET', uri);
    request.send();

    request.onreadystatechange = () => {
        if(request.readyState == XMLHttpRequest.DONE && request.status == 200) {
            let parsedData = JSON.parse(request.responseText);
            if(parsedData.isRequestOk) {
                let caseAssign = document.querySelector(`#id${parsedData.bugId} .case-assign`);
                let caseClose = document.querySelector(`#id${parsedData.bugId} .case-close`);
                switch(parsedData.action) {
                    case 'assign':
                        caseAssign.innerHTML = parsedData.assignedTo;
                        caseClose.innerHTML = `<a class="waves-effect waves-light btn-small" id="close" onclick="update(this, ${parsedData.bugId})">Clôturer</a>`;
                        break;
                    case 'close':
                        caseClose.innerHTML = parsedData.closeDate.replace(/^(\d+)-(\d+)-(\d+).*/, "$3/$2/$1");
                        break;

                }
            } else {
                alert(parsedData.reason);
            }
        }
    };

}
    