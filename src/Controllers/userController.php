<?php

namespace BugApp\Controllers;

use BugApp\Models\UserManager;
use BugApp\Controllers\abstractController;

class UserController extends abstractController
{

    public function login() {

        $manager = new UserManager();

        if(isset($_POST['submit'])) {

            //On stocke les valeurs de post pour plus de clareté
            $email = $_POST['user'];
            $password = $_POST['pwd'];
            $user = $manager->findByEmail($email);

            /*check bdd*/
            if($user != null) {
                $check = $manager->check($user, $password);
                /*check bdd mdp*/
                if($check) {
                    //On regarde le type de l'user
                    switch(get_class($user)) {
                        case 'BugApp\Models\Engineer':
                            $_SESSION['user'] = $user;
                            $_SESSION['type'] = 'engineer';
                            //header a verifier à nouveau
                            header('Location: '.PUBLIC_PATH.'bug');
                            break;
                        case 'BugApp\Models\Recorder':
                            $_SESSION['user'] = $user;
                            $_SESSION['type'] = 'recorder';
                            //header a verifier à nouveau
                            header('Location: '.PUBLIC_PATH.'bug');
                            break;   
                    }
                } else {
                    $content = $this->render('src/Views/User/login', ['error' => "Mot de passe incorrect."]);

                    return $this->sendHttpResponse($content, 200);
                }
            } else {
                $content = $this->render('src/Views/User/login', ['error' => "L'utilisateur n'existe pas."]);

                return $this->sendHttpResponse($content, 200);
            }
        } else {

            $content = $this->render('src/Views/User/login', []);

            return $this->sendHttpResponse($content, 200);

        }

    }

    public function logout() {
        session_destroy();
        unset($_SESSION);
        header('Location: '.PUBLIC_PATH.'login');
    }

}
