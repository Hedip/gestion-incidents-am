SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `bug`;
CREATE TABLE `bug` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `closed` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `bug` (`id`, `title`, `description`, `createdAt`, `closed`) VALUES
(1,	'Echec d\'envoi du formulaire',	'Incident sur le site web : Aliquam iaculis justo non quam commodo suscipit. Proin vestibulum massa ut consequat pharetra. Morbi sollicitudin pharetra euismod. Curabitur pretium, augue a commodo mattis, dolor odio congue nisi, ut commodo odio massa eget massa. Fusce non orci fermentum, dignissim metus nec, fringilla orci. Ut condimentum eget erat vitae vestibulum. In sollicitudin, ligula ac maximus luctus, mi nisi semper quam, eu cursus sapien orci vel magna. Aenean velit lorem, vestibulum eu finibus ut, mattis rhoncus nibh. Aliquam eleifend mollis augue sit amet lacinia.',	'2020-11-30 17:09:38',	null);


