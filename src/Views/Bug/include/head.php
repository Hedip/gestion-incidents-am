<?php
echo '
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
    ';

// Détermination de la requête
$length = strlen(BASE_PATH);
$uri = substr($_SERVER['REQUEST_URI'], $length+1) ;
    
switch ($uri) {
    case 'add':
        echo '<title>Nouvel incident</title>';
        break;
    case 'bug':
        echo '<title>Liste des incidents</title>';
        break;
    default:
        echo '<title>Détails incident</title>';
}

echo '
    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection" />
    <link href="/css/style.css" type="text/css" rel="stylesheet" media="screen,projection" />
    ';
?>