<?php

namespace BugApp\Models;

use BugApp\Services\Manager;

class BugManager extends Manager
{

    public function find($id)
    {

        // Connexion à la BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT * FROM bug WHERE id = :id');
        $sth->bindParam(':id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setRecorder($this->findUser($result["recorder_id"], 'recorder'));
        $bug->setEngineer($this->findUser($result["engineer_id"], 'engineer'));
        $bug->setClosedAt($result["closed"]);

        // Retour
        return $bug;
    }

    

    public function findAll($title = '')
    {
        $like_title = "%$title%";
        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        $req_find_all = $dbh->prepare('SELECT * FROM bug WHERE title LIKE :title');
        $req_find_all->bindParam(':title', $like_title, \PDO::PARAM_STR);
        $req_find_all->execute();
        
        $bugs = [];

        while($row = $req_find_all->fetch(\PDO::FETCH_ASSOC)) {
            // Instanciation d'un bug
            $bug = new Bug();
            $bug->setId($row["id"]);
            $bug->setTitle($row["title"]);
            $bug->setDescription($row["description"]);
            $bug->setCreatedAt($row["createdAt"]);
            $bug->setRecorder($this->findUser($row["recorder_id"], 'recorder'));
            $bug->setEngineer($this->findUser($row["engineer_id"], 'engineer'));
            
            $bug->setClosedAt($row["closed"]);

            array_push($bugs, $bug);
        }
        return $bugs;

    }

    public function findAllNotClosed($title)
    {

        $like_title = "%$title%";
        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        $req_find_all_not_closed = $dbh->prepare('SELECT * FROM bug WHERE closed IS NULL AND title LIKE :title');
        $req_find_all_not_closed->bindParam(':title', $like_title, \PDO::PARAM_STR);
        $req_find_all_not_closed->execute();
        
        $bugs = [];

        while($row = $req_find_all_not_closed->fetch(\PDO::FETCH_ASSOC)) {
            // Instanciation d'un bug
            $bug = new Bug();
            $bug->setId($row["id"]);
            $bug->setTitle($row["title"]);
            $bug->setDescription($row["description"]);
            $bug->setCreatedAt($row["createdAt"]);

            $bug->setRecorder($this->findUser($row["recorder_id"], 'recorder'));
            $bug->setEngineer($this->findUser($row["engineer_id"], 'engineer'));
            
            $bug->setClosedAt($row["closed"]);

            array_push($bugs, $bug);
        }
        return $bugs;

    }

    public function findAllAssignedToEngineer($engineer_id, $title)
    {

        $like_title = "%$title%";
        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        $req_find_all_assigned = $dbh->prepare('SELECT * FROM bug WHERE engineer_id = :engineer_id AND title LIKE :title');
        $req_find_all_assigned->bindParam(':engineer_id', $engineer_id, \PDO::PARAM_INT);
        $req_find_all_assigned->bindParam(':title', $like_title, \PDO::PARAM_STR);
        $req_find_all_assigned->execute();
        
        $bugs = [];

        while($row = $req_find_all_assigned->fetch(\PDO::FETCH_ASSOC)) {
            // Instanciation d'un bug
            $bug = new Bug();
            $bug->setId($row["id"]);
            $bug->setTitle($row["title"]);
            $bug->setDescription($row["description"]);
            $bug->setCreatedAt($row["createdAt"]);

            $bug->setRecorder($this->findUser($row["recorder_id"], 'recorder'));
            $bug->setEngineer($this->findUser($row["engineer_id"], 'engineer'));
            
            $bug->setClosedAt($row["closed"]);

            array_push($bugs, $bug);
        }
        return $bugs;

    }

    public function findAllNotClosedAssignedToEngineer($engineer_id, $title) {
        $like_title = "%$title%";
        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        $req_find_all_assigned_not_closed = $dbh->prepare('SELECT * FROM bug WHERE engineer_id = :engineer_id AND closed IS NULL AND title LIKE :title');
        $req_find_all_assigned_not_closed->bindParam(':engineer_id', $engineer_id, \PDO::PARAM_INT);
        $req_find_all_assigned_not_closed->bindParam(':title', $like_title, \PDO::PARAM_STR);
        $req_find_all_assigned_not_closed->execute();
        
        $bugs = [];

        while($row = $req_find_all_assigned_not_closed->fetch(\PDO::FETCH_ASSOC)) {
            // Instanciation d'un bug
            $bug = new Bug();
            $bug->setId($row["id"]);
            $bug->setTitle($row["title"]);
            $bug->setDescription($row["description"]);
            $bug->setCreatedAt($row["createdAt"]);
            
            
            $bug->setRecorder($this->findUser($row["recorder_id"], 'recorder'));
            $bug->setEngineer($this->findUser($row["engineer_id"], 'engineer'));

            $bug->setClosedAt($row["closed"]);

            array_push($bugs, $bug);
        }
        return $bugs;
    }

    public function findUser($id, $type) {

        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        $requete = null;
        $user = null;

        switch($type) {
            case 'recorder':
                $requete = "SELECT user.id, user.nom, user.email, user.type FROM user, recorder WHERE recorder.user_id = user.id AND recorder.id = :id";
                $user = new Recorder();
                break;
            case 'engineer':
                $requete = "SELECT user.id, user.nom, user.email, user.type FROM user, engineer WHERE engineer.user_id = user.id AND engineer.id = :id";
                $user = new Engineer();
                break;
        }
        
        $req_find_user = $dbh->prepare($requete);
        $req_find_user->bindParam(':id', $id, \PDO::PARAM_INT);
        $req_find_user->execute();

        $result = $req_find_user->fetch(\PDO::FETCH_ASSOC);

        if(empty($result)) {
            return null;
        }
        $user->setId($result["id"]);
        $user->setNom($result["nom"]);
        $user->setEmail($result["email"]);

        return $user;
    }

    public function findEngineerId($user_id) {

        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        $req_find_user = $dbh->prepare("SELECT id
                                        FROM engineer
                                        WHERE engineer.user_id = :id");
        $req_find_user->bindParam(':id', $user_id, \PDO::PARAM_INT);
        $req_find_user->execute();

        $result = $req_find_user->fetch(\PDO::FETCH_ASSOC);

        if(empty($result)) {
            return null;
        }

        return $result["id"];
    }

    /**
     * Pas de test de type de session, les requêtes doivent juste REQUETER ET RIEN D'AUTRE
     */
    public function findAllFromRecorder($user_id)
    {

        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        $req_find_all_from_recorder = $dbh->prepare('SELECT bug.id, bug.title, bug.description, bug.createdAt, bug.closed
                                                    FROM bug, recorder
                                                    WHERE bug.recorder_id = recorder.id AND recorder.user_id = :user_id');
        $req_find_all_from_recorder->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
        $req_find_all_from_recorder->execute();
        
        $bugs = [];

        while($row = $req_find_all_from_recorder->fetch(\PDO::FETCH_ASSOC)) {
            // Instanciation d'un bug
            $bug = new Bug();
            $bug->setId($row["id"]);
            $bug->setTitle($row["title"]);
            $bug->setDescription($row["description"]);
            $bug->setCreatedAt($row["createdAt"]);
            $bug->setClosedAt($row["closed"]);

            array_push($bugs, $bug);
        }
        return $bugs;

    }

    public function add(Bug $bug, Recorder $user){

        // Ajout d'un incident en BDD
        $dbh = static::connectDb();

        $bug_title = $bug->getTitle();
        $bug_description = $bug->getDescription();
        $bug_created_at =$bug->getCreatedAt()->format("Y-m-d H:i:s");
        $user_id = $user->getId();

        $req_insert = $dbh->prepare('INSERT INTO bug (title, description, createdAt, recorder_id)
                                    SELECT :bug_title, :bug_description, :bug_created_at, recorder.id
                                    FROM user, recorder
                                    WHERE user.id = recorder.user_id AND recorder.user_id = :user_id');
        $req_insert->bindParam(':bug_title', $bug_title, \PDO::PARAM_STR);
        $req_insert->bindParam(':bug_description', $bug_description, \PDO::PARAM_STR);
        $req_insert->bindParam(':bug_created_at', $bug_created_at, \PDO::PARAM_STR);
        $req_insert->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
        $req_insert->execute();

    }

    public function update_close(Bug $bug){

       // Modification d'un incident en BDD
       $dbh = static::connectDb();

       $bug_id = $bug->getId();
       $today = date('Y-m-d H:i:s');

       $req_insert = $dbh->prepare('UPDATE bug
                                   SET closed = :today
                                   WHERE bug.id = :bug_id');
       $req_insert->bindParam(':today', $today, \PDO::PARAM_STR);
       $req_insert->bindParam(':bug_id', $bug_id, \PDO::PARAM_INT);
       $req_insert->execute();

       return $today;
    }

    public function update_assign(Bug $bug, Engineer $user){

        // Modification d'un incident en BDD
        $dbh = static::connectDb();
 
        $bug_id = $bug->getId();
        $engineer_id = $this->findEngineerId($user->getId());
 
        $req_insert = $dbh->prepare('UPDATE bug
                                    SET engineer_id = :engineer_id
                                    WHERE bug.id = :bug_id');
        $req_insert->bindParam(':engineer_id', $engineer_id, \PDO::PARAM_INT);
        $req_insert->bindParam(':bug_id', $bug_id, \PDO::PARAM_INT);
        $req_insert->execute();
 
     }







}
