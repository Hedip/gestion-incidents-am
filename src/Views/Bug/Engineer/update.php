<?php

    /** @var $bug \BugApp\Models\Bug */

    $bug = $parameters['bug'];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php 
        $path_include = "../src/Views/bug/include/";
        include($path_include.'head.php')
    ?>
</head>

<body>

  <?php 
    include($path_include.'navbar.php');
  ?>

  <main>
    <div class="container">
      <br><br>
      <a class="waves-effect waves-light btn-large" href="<?= PUBLIC_PATH; ?>bug"><i class="material-icons left">arrow_back</i>retour à la liste</a>

      <h4 class="header teal-text">Fiche descriptive d'incident</h4>
      <br>
      <div class="row">
        <div class="col l3">
          <h5 class="header teal-text">Nom de l'incident : </h5>
        </div>
        <div class="col l9">
          <h5 class="header teal-text text-darken-2"><?=$bug->getTitle();?></h5>
        </div>
      </div>
      <div class="row">
        <div class="col l3">
          <h5 class="header teal-text">Date d'observation :</h5>
        </div>
        <div class="col l9">
          <h5 class="header teal-text text-darken-2"><?php echo $bug->getCreatedAt()->format("d/m/Y");?></h5>
        </div>
      </div>

      <div class="row">
        <div class="col l3">
          <h5 class="header teal-text">Date de cloture : </h5>
        </div>
        <div class="col l9">
          <h5 class="header teal-text text-darken-2">
            <?php 
                if($bug->getClosedAt() != null){
                    echo $bug->getClosedAt()->format("d/m/Y");
                } else {
                    echo 'Pas encore traité';
                }
            ?>
          </h5>
        </div>
      </div>

      <div class="row">
        <div class="col l3">
          <h5 class="header teal-text">Description de l'incident : </h5>
        </div>
        <div class="col l9">
          <p class="teal-text text-darken-2 justify"><?=$bug->getDescription();?></p>
        </div>
      </div>

      <br><br>  
      
      <div class="row">
        <form method="post" class="col s12">
          <div class="col l6">
            <label>
            <input type="checkbox" class="filled-in" name="cloture"/>
            <span>Fermer l'incident</span>
            </label>
          </div>
          <div class="col l6">
            <button class="waves-effect waves-light btn" type="submit" name="submit"><i class="material-icons left">send</i>Soumettre</button>
          </div>
        </form>
      </div>
      
      <br><br>

      
    </div>
  </main>

  <?php 
    include($path_include.'footer.php');
  ?>



  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="/js/materialize.js"></script>

</body>

</html>