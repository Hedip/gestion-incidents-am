<?php

require('../src/init.php');

session_start();

use BugApp\Controllers\bugController;
use BugApp\Controllers\userController;

switch(true) {

    
    case ($uri == 'login'):
        $controller = new userController();

        return $controller->login();
        
        break;
    
    case (!isset($_SESSION['user'])) :

        header('Location: '.PUBLIC_PATH.'login');

        break;

    case preg_match('#^bug/show/(\d+)$#', $uri, $matches):

        $id = $matches[1];

        $controller = new bugController();

        return $controller->show($id);

        break;

    case preg_match('#^bug/update/(\d+)$#', $uri, $matches):

        $id = $matches[1];

        $controller = new bugController();

        return $controller->update($id);

        break;

    case preg_match('#^bug/updateajax/(\d+)\?([a-z]*=(true|false))&([a-z]*=(true|false))$#', $uri, $matches):

        $id = $matches[1];

        $controller = new bugController();

        return $controller->updateAjax($id);

        break;

    case preg_match('#^bug/filter\?([a-z]*=(true|false)&){2}query=.*$#', $uri, $matches):

        $controller = new bugController();

        return $controller->filter();

        break;
    
    case preg_match('#^bug((\?)|$)#', $uri):

        $controller = new bugController();

        return $controller->index();

        break;
    
    case ($uri == 'bug/add'):

        $controller = new bugController();
        
        return $controller->add();
        
        break;

    case ($uri == 'logout'):
        $controller = new userController();

        return $controller->logout();
        
        break;

    default:
    
    http_response_code(404);
    
    echo "<h1>Gestion d'incidents</h1><p>ERREUR 404 - Page introuvable</p>";
    
}

?>