<?php

namespace BugApp\Controllers;

use BugApp\Models\BugManager;
use BugApp\Models\Bug;
use BugApp\Controllers\abstractController;

class bugController extends abstractController
{

    public function show($id)
    {

        // Données issues du Modèle

        $manager = new BugManager();

        $bug = $manager->find($id);

        // Template issu de la Vue
        $content = $this->render('src/Views/Bug/'.$_SESSION['type'].'/show', ['bug' => $bug]);

        return $this->sendHttpResponse($content, 200);
    }

    public function index()
    {
        $manager = new BugManager();

        $bugs = null;

        $user = $_SESSION['user'];
        $user_type = $_SESSION['type'];

        switch($user_type) {
            case 'recorder':
                $bugs = $manager->findAllFromRecorder($user->getId());
                break;
            case 'engineer':
                $bugs = $manager->findAll();
                break;
        }

        // TODO: liste des incidents

        $content = $this->render('src/Views/Bug/'.$_SESSION['type'].'/list', ['bugs' => $bugs, $user_type => $user]);

        return $this->sendHttpResponse($content, 200);
    }

    public function add()
    {

        // Ajout d'un incident

        // TODO: ajout d'incident (GET et POST)
        $manager = new BugManager();
        
        if(isset($_POST['submit'])) {
            $user = $_SESSION['user'];

            $bug = new Bug();
            $bug->setTitle($_POST['title']);
            $bug->setDescription($_POST['description']);
            $heure = $this->format_time($_POST['heure']);
            $minutes = $this->format_time($_POST['minutes']);
            $bug->setCreatedAt($_POST['date'].' '.$heure.':'.$minutes.':00');

            $manager->add($bug, $user);

            header('Location: '.PUBLIC_PATH.'bug');
        } else {
            $content = $this->render('src/Views/Bug/Recorder/add', []);
            return $this->sendHttpResponse($content, 200);
        }
    }

    public function update($bug_id) {
        
        $manager = new BugManager();

        $bug = $manager->find($bug_id);

        //Si l'incident a déjà une date de cloture on redirige vers la page show de cet incident
        if($bug->getClosedAt() != null || $_SESSION['type'] != 'engineer') {
            header('Location: '.PUBLIC_PATH.'bug/show/'.$bug_id);
            return;
        }

        //TODO bouton assign
        if(isset($_POST['submit']) && isset($_POST['cloture']) && $_POST['cloture'] == true) {
            
            $manager->update_close($bug);

            header('Location: '.PUBLIC_PATH.'bug/show/'.$bug_id);
        } else {
            $content = $this->render('src/Views/Bug/Engineer/update', ['bug' => $bug]);
            return $this->sendHttpResponse($content, 200);
        }
    }

    public function updateAjax($bug_id) {

        if(!isset($_GET) || !isset($_GET['assign']) || !isset($_GET['close']) || $_SESSION['type'] != 'engineer') {
            echo json_encode(["isRequestOk" => false, "reason" => "wrong arguments passed."]);
            return;
        }

        $manager = new BugManager();

        $bug = $manager->find($bug_id);

        $getAssign = ($_GET['assign'] == "true");
        $getClose = ($_GET['close'] == "true");
        $connectedEngineer = $_SESSION['user'];

        if($getAssign && null != $bug->getEngineer()) {
            echo json_encode(["isRequestOk" => false, "reason" => "Incident déjà assigné."]);
            return;
        }

        if($getClose && null != $bug->getClosedAt()) {
            echo json_encode(["isRequestOk" => false, "reason" => "Incident déjà clôturé."]);
            return;
        }

        if($getClose && $bug->getEngineer()->getId() != $connectedEngineer->getId()) {
            echo json_encode(["isRequestOk" => false, "reason" => "Vous ne pouvez pas clôturer cet incident."]);
            return;
        }

        if($getAssign) {
            $manager->update_assign($bug, $connectedEngineer);
            echo json_encode(["isRequestOk" => true, "action" => "assign", "assignedTo" => $connectedEngineer->getNom(), "bugId" => $bug->getId()]);
            return;
        }
        if($getClose) {
            $closeDate = $manager->update_close($bug);
            echo json_encode(["isRequestOk" => true, "action" => "close", "closeDate" => $closeDate, "bugId" => $bug->getId()]);
            return;
        }

        echo json_encode(["isRequestOk" => false, "reason" => "No action performed."]);
        return;
    }

    public function filter() {

        $manager = new BugManager();

        if(!isset($_GET) || !isset($_GET['assigned']) || !isset($_GET['notclosed'])) {
            echo json_encode(["isRequestOk" => false]);
            return;
        }

        $getAssigned = ($_GET['assigned'] == "true");
        $getClosed = ($_GET['notclosed'] == "true");
        $query = (!empty($_GET['query'])) ? $_GET["query"] : "";
        $user_id = $_SESSION['user']->getId();

        $engineer_id = $manager->findEngineerId($user_id);

        $requestOkArray = ["isRequestOk" => true];

        switch(true) {

            case ($getAssigned && $getClosed):
                echo json_encode(array_merge(
                                                $requestOkArray, 
                                                ["results" => $manager->findAllNotClosedAssignedToEngineer($engineer_id, $query)],
                                                ["connectedUserId" => $user_id]
                                ));
                return;

            case $getClosed:
                echo json_encode(array_merge($requestOkArray, ["results" => $manager->findAllNotClosed($query)], ["connectedUserId" => $user_id] ));
                return;

            case $getAssigned:
                echo json_encode(array_merge(
                                                $requestOkArray, 
                                                ["results" => $manager->findAllAssignedToEngineer($engineer_id, $query)],
                                                ["connectedUserId" => $user_id]
                                ));
                return;

            default:
                echo json_encode(array_merge(
                                                $requestOkArray,
                                                ["results" => $manager->findAll($query)],
                                                ["connectedUserId" => $user_id]
                                ));
                

        }
    }

    private function format_time($time) {
        if(strlen($time) == 0) {
            return "00";
        } elseif(strlen($time) == 1) {
            return "0" + $time;
        }
        return $time;
    }

}

?>