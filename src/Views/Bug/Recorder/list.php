<?php

$bugs = $parameters['bugs'];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php 
        $path_include = "../src/Views/bug/include/";
        include($path_include.'head.php');
    ?>
</head>

<body>

  <?php 
    include($path_include.'navbar.php');
  ?>

  <main>
    <div class="container">
      <h1 class="header center teal-text lighten-1">Liste de vos incidents</h1>
      <br><br>
      <a class="waves-effect waves-light btn-large" href="<?= PUBLIC_PATH; ?>bug/add"><i class="material-icons left">add</i>Ajouter un incident</a>
      <br><br>
      <table class="centered responsive-table">

        <thead>
          <tr>
            <th>ID</th>
            <th>Sujet</th>
            <th>Date</th>
            <th>Clotûre</th>
            <th></th>
          </tr>
        </thead>

        
        <tbody>

        <?php
            foreach($bugs as $currentBug) {
                echo '
                <tr>
                    <td>'.$currentBug->getId().'</td>
                    <td>'.$currentBug->getTitle().'</td>
                    <td>'.$currentBug->getCreatedAt()->format("d/m/Y").'</td>
                    <td>'.($currentBug->getClosedAt() != null ? $currentBug->getClosedAt()->format("d/m/Y") : "").'<td>
                    <td><a class="waves-effect waves-light btn-small" href="'.PUBLIC_PATH.'bug/show/'.$currentBug->getId().'"><i class="material-icons left">subject</i>détails...</a></td>
                    </tr>
                ';
            }
        ?>
        </tbody>
      </table>
      <br><br><br>
    </div>
  </main>
  

  <?php 
    include($path_include.'footer.php');
  ?>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/init.js"></script>


</body>

</html>