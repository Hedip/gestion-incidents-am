<?php

namespace BugApp\Models;

use JsonSerializable;

class Engineer extends User implements JsonSerializable {

    protected $id;


    public function getId()
    {
        return $this->id;
    }


    public function setId($id)
    {
        $this->id = $id;
    }

    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'nom' => $this->getNom(),
            'email' => $this->getEmail()
        ];
    }

}