<?php

//echo var_dump($parameters);
$bugs = $parameters['bugs'];
$connectedEngineer = $parameters['engineer'];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php 
        $path_include = "../src/Views/bug/include/";
        include($path_include.'head.php');
    ?>
</head>

<body>

  <?php 
    include($path_include.'navbar.php');
  ?>

  <main>
    <div class="container">
      <h1 class="header center teal-text lighten-1">Liste des incidents</h1>
      <br><br>

      <div class="row">
        <div class="col l6">
          <label>
          <input type="checkbox" class="filled-in" name="afficher_cloture" id="notclosed"/>
          <span>Afficher seulement les incidents non clôturés.</span>
          </label>
        </div>
      </div>

      <div class="row">
        <div class="col l6">
          <label>
          <input type="checkbox" class="filled-in" name="afficher_assigne" id="assigned"/>
          <span>Afficher seulement les incidents qui vous sont assignés.</span>
          </label>
        </div>
      </div>

      <div class="row">
        <div class="col l6">
          <div class="input-field col s10">
            <i class="material-icons prefix">search</i>
            <input id="search" type="text" class="validate" placeholder="Titre de l'incident...">
          </div>
        </div>
      </div>
      <br>

      <table class="centered responsive-table">

        <thead>
          <tr>
            <th width="8%">ID</th>
            <th width="17%">Sujet</th>
            <th width="15%">Date</th>
            <th width="15%">Client</th>
            <th width="17%"></th>
            <th width="15%">Ingénieur</th>
            <th width="15%">Clotûre</th>
          </tr>
        </thead>

        
        <tbody id="listbody">

        <?php
            foreach($bugs as $currentBug) {

                if(null != $currentBug->getClosedAt()) {
                  $valeurCaseCloture = $currentBug->getClosedAt()->format("d/m/Y");
                } else if(null == $currentBug->getEngineer()) {
                  $valeurCaseCloture = "Non assigné";
                } else if($currentBug->getEngineer()->getId() == $connectedEngineer->getId()) {
                  $valeurCaseCloture = '<a class="waves-effect waves-light btn-small" id="close" onclick="update(this,'.$currentBug->getId().')">Clôturer</a>';
                } else {
                  $valeurCaseCloture = "En cours";
                }

                echo '
                  <tr class="bug" id="id'.$currentBug->getId().'">
                    <td>'.$currentBug->getId().'</td>
                    <td>'.$currentBug->getTitle().'</td>
                    <td>'.$currentBug->getCreatedAt()->format("d/m/Y").'</td>
                    <td>'.$currentBug->getRecorder()->getNom().'</td>
                    <td><a class="waves-effect waves-light btn-small" href="'.PUBLIC_PATH.'bug/show/'.$currentBug->getId().'"><i class="material-icons left">subject</i>détails...</a></td>
                    <td class="case-assign">'.($currentBug->getEngineer() != null ? $currentBug->getEngineer()->getNom() : '<a class="waves-effect waves-light btn-small" id="assign" onclick="update(this,'.$currentBug->getId().')">Assigner</a>').'</td>
                    <td class="case-close">'.$valeurCaseCloture.'<td>
                  </tr>
                ';
            }
        ?>
        </tbody>
      </table>
      <br><br><br>
    </div>
  </main>
  

  <?php 
    include($path_include.'footer.php');
  ?>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/script.js"></script>


</body>

</html>