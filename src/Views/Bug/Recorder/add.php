<!DOCTYPE html>
<html lang="en">

<head>
    <?php
        $path_include = "../src/Views/bug/include/";
        include($path_include.'head.php');
    ?>
</head>

<body>

  <?php 
    include($path_include.'navbar.php');
  ?>
  <main>
  <div class="container">
    <br><br>
    <a class="waves-effect waves-light btn-large" href="<?= PUBLIC_PATH; ?>bug"><i class="material-icons left">arrow_back</i>retour à la liste</a>
    <h3 class="header center teal-text lighten-1">Rapport d'incident</h>
    <br><br>
    <div class="row center">
      <div class="row">
          <form method="post" class="col s12">
          <div class="row">
              <div class="input-field col s12">
                <input required placeholder="" id="titre" type="text" class="validate" name="title">
                <label for="titre">Titre de l'incident :</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s6">
                <input required id="date" type="date" class="validate" value="<?php echo date("Y-m-d");?>" name="date">
                <label for="date">Date de l'incident :</label>
              </div>
              <div class="input-field col s3">
                <input required placeholder="" id="heure" value="<?php echo date("H")?>" type="number" min="0" max="23" class="validate" name="heure">
                <label for="titre">Heure :</label>
              </div>
              <div class="input-field col s3">
                <input required placeholder="" id="minutes" value="<?php echo date("i")?>" type="number" min="0" max="59" class="validate" name="minutes">
                <label for="titre">Minutes :</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <textarea required id="description" class="materialize-textarea" placeholder="" name="description"></textarea>
                <label for="description">Description de l'incident :</label>
              </div>
            </div>
            <div class="row">
              <button class="waves-effect waves-light btn" type="submit" name="submit"><i class="material-icons left">send</i>Soumettre</button>
            </div>
          </form>
        </div>
  </div>
  </main>

  <?php 
    include($path_include.'footer.php');
  ?>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/init.js"></script>

  </body>
</html>
